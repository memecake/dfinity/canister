import { useState, useEffect, useCallback } from "react";
import {
  Connection,
  PublicKey,
  Transaction,
  clusterApiUrl,
  SystemProgram,
} from "@solana/web3.js";
import "./styles.css";
import * as solanaWeb3 from '@solana/web3.js';
import * as splToken from '@solana/spl-token';

import { Principal } from '@dfinity/principal';
import {StoicIdentity} from "ic-stoic-identity";
import { Actor, HttpAgent } from '@dfinity/agent';
import { IDL } from "@dfinity/candid";
import {getMarketPlaceId, mktTransferNFT} from './marketplace';
import {getTokenIdentifier, getAccountIdentifier} from './identifier-utils';

type DisplayEncoding = "utf8" | "hex";
type PhantomEvent = "disconnect" | "connect" | "accountChanged";
type PhantomRequestMethod =
  | "connect"
  | "disconnect"
  | "signTransaction"
  | "signAllTransactions"
  | "signMessage";

interface ConnectOpts {
  onlyIfTrusted: boolean;
}

interface PhantomProvider {
  publicKey: PublicKey | null;
  isConnected: boolean | null;  
  signAllTransactions: (transactions: Transaction[]) => Promise<Transaction[]>;  
  connect: (opts?: Partial<ConnectOpts>) => Promise<{ publicKey: PublicKey }>;
  disconnect: () => Promise<void>;
  on: (event: PhantomEvent, handler: (args: any) => void) => void;
  request: (method: PhantomRequestMethod, params: any) => Promise<unknown>;
}

const getProvider = (): PhantomProvider | undefined => {
  if ("solana" in window) {
    const anyWindow: any = window;
    const provider = anyWindow.solana;
    if (provider.isPhantom) {
      return provider;
    }
  }
  window.open("https://phantom.app/", "_blank");
};

const NETWORK = clusterApiUrl("devnet");

const mWindow: any = window;
const nnsCanisterId = 'ryjl3-tyaaa-aaaaa-aaaba-cai'
const boxyId = "s36wu-5qaaa-aaaah-qcyzq-cai" //"txr2a-fqaaa-aaaah-qcmkq-cai";
const nnsPartialInterfaceFactory = ({ IDL }) => {
  const BlockIndex = IDL.Nat64;
  const AccountIdentifier = IDL.Vec(IDL.Nat8);
  const Tokens = IDL.Record({ 'e8s' : IDL.Nat64 });
  const Memo = IDL.Nat64;
  const SubAccount = IDL.Vec(IDL.Nat8);
  const TimeStamp = IDL.Record({ 'timestamp_nanos' : IDL.Nat64 });
  const TransferArgs = IDL.Record({
    'to' : AccountIdentifier,
    'fee' : Tokens,
    'memo' : Memo,
    'from_subaccount' : IDL.Opt(SubAccount),
    'created_at_time' : IDL.Opt(TimeStamp),
    'amount' : Tokens,
  });
  const TransferError = IDL.Variant({
    'TxTooOld' : IDL.Record({ 'allowed_window_nanos' : IDL.Nat64 }),
    'BadFee' : IDL.Record({ 'expected_fee' : Tokens }),
    'TxDuplicate' : IDL.Record({ 'duplicate_of' : BlockIndex }),
    'TxCreatedInFuture' : IDL.Null,
    'InsufficientFunds' : IDL.Record({ 'balance' : Tokens }),
  });
  const TransferResult = IDL.Variant({
    'Ok' : BlockIndex,
    'Err' : TransferError,
  });
  return IDL.Service({
    'transfer' : IDL.Func([TransferArgs], [TransferResult], []),
  });
};
const DIP20IDL = ({ IDL }) => {
  const Vec = IDL.Rec();
  const InitArgs = IDL.Record({
    'cap' : IDL.Opt(IDL.Principal),
    'logo' : IDL.Opt(IDL.Text),
    'name' : IDL.Opt(IDL.Text),
    'custodians' : IDL.Opt(IDL.Vec(IDL.Principal)),
    'symbol' : IDL.Opt(IDL.Text),
  });
  const NftError = IDL.Variant({
    'UnauthorizedOperator' : IDL.Null,
    'SelfTransfer' : IDL.Null,
    'TokenNotFound' : IDL.Null,
    'UnauthorizedOwner' : IDL.Null,
    'SelfApprove' : IDL.Null,
    'OperatorNotFound' : IDL.Null,
    'ExistedNFT' : IDL.Null,
    'OwnerNotFound' : IDL.Null,
  });
  const Result = IDL.Variant({ 'Ok' : IDL.Nat, 'Err' : NftError });
  const Result_1 = IDL.Variant({ 'Ok' : IDL.Bool, 'Err' : NftError });
  const Metadata = IDL.Record({
    'logo' : IDL.Opt(IDL.Text),
    'name' : IDL.Opt(IDL.Text),
    'created_at' : IDL.Nat64,
    'upgraded_at' : IDL.Nat64,
    'custodians' : IDL.Vec(IDL.Principal),
    'symbol' : IDL.Opt(IDL.Text),
  });
  Vec.fill(
    IDL.Vec(
      IDL.Tuple(
        IDL.Text,
        IDL.Variant({
          'Nat64Content' : IDL.Nat64,
          'Nat32Content' : IDL.Nat32,
          'BoolContent' : IDL.Bool,
          'Nat8Content' : IDL.Nat8,
          'Int64Content' : IDL.Int64,
          'IntContent' : IDL.Int,
          'NatContent' : IDL.Nat,
          'Nat16Content' : IDL.Nat16,
          'Int32Content' : IDL.Int32,
          'Int8Content' : IDL.Int8,
          'FloatContent' : IDL.Float64,
          'Int16Content' : IDL.Int16,
          'BlobContent' : IDL.Vec(IDL.Nat8),
          'NestedContent' : Vec,
          'Principal' : IDL.Principal,
          'TextContent' : IDL.Text,
        }),
      )
    )
  );
  const GenericValue = IDL.Variant({
    'Nat64Content' : IDL.Nat64,
    'Nat32Content' : IDL.Nat32,
    'BoolContent' : IDL.Bool,
    'Nat8Content' : IDL.Nat8,
    'Int64Content' : IDL.Int64,
    'IntContent' : IDL.Int,
    'NatContent' : IDL.Nat,
    'Nat16Content' : IDL.Nat16,
    'Int32Content' : IDL.Int32,
    'Int8Content' : IDL.Int8,
    'FloatContent' : IDL.Float64,
    'Int16Content' : IDL.Int16,
    'BlobContent' : IDL.Vec(IDL.Nat8),
    'NestedContent' : Vec,
    'Principal' : IDL.Principal,
    'TextContent' : IDL.Text,
  });
  const Result_2 = IDL.Variant({
    'Ok' : IDL.Opt(IDL.Principal),
    'Err' : NftError,
  });
  const ManualReply = IDL.Variant({
    'Ok' : IDL.Vec(IDL.Nat),
    'Err' : NftError,
  });
  const TokenMetadata = IDL.Record({
    'transferred_at' : IDL.Opt(IDL.Nat64),
    'transferred_by' : IDL.Opt(IDL.Principal),
    'owner' : IDL.Opt(IDL.Principal),
    'operator' : IDL.Opt(IDL.Principal),
    'approved_at' : IDL.Opt(IDL.Nat64),
    'approved_by' : IDL.Opt(IDL.Principal),
    'properties' : IDL.Vec(IDL.Tuple(IDL.Text, GenericValue)),
    'is_burned' : IDL.Bool,
    'token_identifier' : IDL.Nat,
    'burned_at' : IDL.Opt(IDL.Nat64),
    'burned_by' : IDL.Opt(IDL.Principal),
    'minted_at' : IDL.Nat64,
    'minted_by' : IDL.Principal,
  });
  const ManualReply_1 = IDL.Variant({
    'Ok' : IDL.Vec(TokenMetadata),
    'Err' : NftError,
  });
  const Stats = IDL.Record({
    'cycles' : IDL.Nat,
    'total_transactions' : IDL.Nat,
    'total_unique_holders' : IDL.Nat,
    'total_supply' : IDL.Nat,
  });
  const SupportedInterface = IDL.Variant({
    'Mint' : IDL.Null,
    'Approval' : IDL.Null,
  });
  const ManualReply_2 = IDL.Variant({ 'Ok' : TokenMetadata, 'Err' : NftError });
  return IDL.Service({
    'approve' : IDL.Func([IDL.Principal, IDL.Nat], [Result], []),
    'balanceOf' : IDL.Func([IDL.Principal], [Result], ['query']),
    'custodians' : IDL.Func([], [IDL.Vec(IDL.Principal)], ['query']),
    'cycles' : IDL.Func([], [IDL.Nat], ['query']),
    'dfxInfo' : IDL.Func([], [IDL.Text], ['query']),
    'gitCommitHash' : IDL.Func([], [IDL.Text], ['query']),
    'isApprovedForAll' : IDL.Func(
        [IDL.Principal, IDL.Principal],
        [Result_1],
        ['query'],
      ),
    'logo' : IDL.Func([], [IDL.Opt(IDL.Text)], ['query']),
    'metadata' : IDL.Func([], [Metadata], ['query']),
    'mint' : IDL.Func(
        [IDL.Principal, IDL.Nat, IDL.Vec(IDL.Tuple(IDL.Text, GenericValue))],
        [Result],
        [],
      ),
    'name' : IDL.Func([], [IDL.Opt(IDL.Text)], ['query']),
    'operatorOf' : IDL.Func([IDL.Nat], [Result_2], ['query']),
    'operatorTokenIdentifiers' : IDL.Func(
        [IDL.Principal],
        [ManualReply],
        ['query'],
      ),
    'operatorTokenMetadata' : IDL.Func(
        [IDL.Principal],
        [ManualReply_1],
        ['query'],
      ),
    'ownerOf' : IDL.Func([IDL.Nat], [Result_2], ['query']),
    'ownerTokenIdentifiers' : IDL.Func(
        [IDL.Principal],
        [ManualReply],
        ['query'],
      ),
    'ownerTokenMetadata' : IDL.Func(
        [IDL.Principal],
        [ManualReply_1],
        ['query'],
      ),
    'rustToolchainInfo' : IDL.Func([], [IDL.Text], ['query']),
    'setApprovalForAll' : IDL.Func([IDL.Principal, IDL.Bool], [Result], []),
    'setCustodians' : IDL.Func([IDL.Vec(IDL.Principal)], [], []),
    'setLogo' : IDL.Func([IDL.Text], [], []),
    'setName' : IDL.Func([IDL.Text], [], []),
    'setSymbol' : IDL.Func([IDL.Text], [], []),
    'stats' : IDL.Func([], [Stats], ['query']),
    'supportedInterfaces' : IDL.Func(
        [],
        [IDL.Vec(SupportedInterface)],
        ['query'],
      ),
    'symbol' : IDL.Func([], [IDL.Opt(IDL.Text)], ['query']),
    'tokenMetadata' : IDL.Func([IDL.Nat], [ManualReply_2], ['query']),
    'totalSupply' : IDL.Func([], [IDL.Nat], ['query']),
    'totalTransactions' : IDL.Func([], [IDL.Nat], ['query']),
    'totalUniqueHolders' : IDL.Func([], [IDL.Nat], ['query']),
    'transfer' : IDL.Func([IDL.Principal, IDL.Nat], [Result], []),
    'transferFrom' : IDL.Func(
        [IDL.Principal, IDL.Principal, IDL.Nat],
        [Result],
        [],
      ),
  });
};

const EXTIDL = ({ IDL }) => {
  const AssetHandle = IDL.Text;
  const SubAccount__1 = IDL.Vec(IDL.Nat8);
  const TokenIndex = IDL.Nat32;
  const AccountIdentifier__1 = IDL.Text;
  const Settlement = IDL.Record({
    'subaccount' : SubAccount__1,
    'seller' : IDL.Principal,
    'buyer' : AccountIdentifier__1,
    'price' : IDL.Nat64,
  });
  const TokenIdentifier = IDL.Text;
  const AccountIdentifier = IDL.Text;
  const User = IDL.Variant({
    'principal' : IDL.Principal,
    'address' : AccountIdentifier,
  });
  const BalanceRequest = IDL.Record({
    'token' : TokenIdentifier,
    'user' : User,
  });
  const Balance = IDL.Nat;
  const CommonError__1 = IDL.Variant({
    'InvalidToken' : TokenIdentifier,
    'Other' : IDL.Text,
  });
  const BalanceResponse = IDL.Variant({
    'ok' : Balance,
    'err' : CommonError__1,
  });
  const TokenIdentifier__1 = IDL.Text;
  const CommonError = IDL.Variant({
    'InvalidToken' : TokenIdentifier,
    'Other' : IDL.Text,
  });
  const Result_7 = IDL.Variant({
    'ok' : AccountIdentifier__1,
    'err' : CommonError,
  });
  const Time = IDL.Int;
  const Listing = IDL.Record({
    'locked' : IDL.Opt(Time),
    'seller' : IDL.Principal,
    'price' : IDL.Nat64,
  });
  const Result_8 = IDL.Variant({
    'ok' : IDL.Tuple(AccountIdentifier__1, IDL.Opt(Listing)),
    'err' : CommonError,
  });
  const Extension = IDL.Text;
  const Metadata = IDL.Variant({
    'fungible' : IDL.Record({
      'decimals' : IDL.Nat8,
      'metadata' : IDL.Opt(IDL.Vec(IDL.Nat8)),
      'name' : IDL.Text,
      'symbol' : IDL.Text,
    }),
    'nonfungible' : IDL.Record({ 'metadata' : IDL.Opt(IDL.Vec(IDL.Nat8)) }),
  });
  const HeaderField = IDL.Tuple(IDL.Text, IDL.Text);
  const HttpRequest = IDL.Record({
    'url' : IDL.Text,
    'method' : IDL.Text,
    'body' : IDL.Vec(IDL.Nat8),
    'headers' : IDL.Vec(HeaderField),
  });
  const HttpStreamingCallbackToken = IDL.Record({
    'key' : IDL.Text,
    'sha256' : IDL.Opt(IDL.Vec(IDL.Nat8)),
    'index' : IDL.Nat,
    'content_encoding' : IDL.Text,
  });
  const HttpStreamingCallbackResponse = IDL.Record({
    'token' : IDL.Opt(HttpStreamingCallbackToken),
    'body' : IDL.Vec(IDL.Nat8),
  });
  const HttpStreamingStrategy = IDL.Variant({
    'Callback' : IDL.Record({
      'token' : HttpStreamingCallbackToken,
      'callback' : IDL.Func(
          [HttpStreamingCallbackToken],
          [HttpStreamingCallbackResponse],
          ['query'],
        ),
    }),
  });
  const HttpResponse = IDL.Record({
    'body' : IDL.Vec(IDL.Nat8),
    'headers' : IDL.Vec(HeaderField),
    'streaming_strategy' : IDL.Opt(HttpStreamingStrategy),
    'status_code' : IDL.Nat16,
  });
  const ListRequest = IDL.Record({
    'token' : TokenIdentifier__1,
    'from_subaccount' : IDL.Opt(SubAccount__1),
    'price' : IDL.Opt(IDL.Nat64),
  });
  const Result_3 = IDL.Variant({ 'ok' : IDL.Null, 'err' : CommonError });
  const Result_6 = IDL.Variant({ 'ok' : Metadata, 'err' : CommonError });
  const Result_5 = IDL.Variant({
    'ok' : IDL.Tuple(AccountIdentifier__1, IDL.Nat64),
    'err' : IDL.Text,
  });
  const Result_4 = IDL.Variant({ 'ok' : IDL.Null, 'err' : IDL.Text });
  const SaleTransaction = IDL.Record({
    'time' : Time,
    'seller' : IDL.Principal,
    'tokens' : IDL.Vec(TokenIndex),
    'buyer' : AccountIdentifier__1,
    'price' : IDL.Nat64,
  });
  const SaleSettings = IDL.Record({
    'startTime' : Time,
    'whitelist' : IDL.Bool,
    'totalToSell' : IDL.Nat,
    'sold' : IDL.Nat,
    'bulkPricing' : IDL.Vec(IDL.Tuple(IDL.Nat64, IDL.Nat64)),
    'whitelistTime' : Time,
    'salePrice' : IDL.Nat64,
    'remaining' : IDL.Nat,
    'price' : IDL.Nat64,
  });
  const Sale = IDL.Record({
    'expires' : Time,
    'subaccount' : SubAccount__1,
    'tokens' : IDL.Vec(TokenIndex),
    'buyer' : AccountIdentifier__1,
    'price' : IDL.Nat64,
  });
  const Balance__1 = IDL.Nat;
  const Result_2 = IDL.Variant({ 'ok' : Balance__1, 'err' : CommonError });
  const Result_1 = IDL.Variant({
    'ok' : IDL.Vec(TokenIndex),
    'err' : CommonError,
  });
  const Result = IDL.Variant({
    'ok' : IDL.Vec(
      IDL.Tuple(TokenIndex, IDL.Opt(Listing), IDL.Opt(IDL.Vec(IDL.Nat8)))
    ),
    'err' : CommonError,
  });
  const Transaction = IDL.Record({
    'token' : TokenIdentifier__1,
    'time' : Time,
    'seller' : IDL.Principal,
    'buyer' : AccountIdentifier__1,
    'price' : IDL.Nat64,
  });
  const Memo = IDL.Vec(IDL.Nat8);
  const SubAccount = IDL.Vec(IDL.Nat8);
  const TransferRequest = IDL.Record({
    'to' : User,
    'token' : TokenIdentifier,
    'notify' : IDL.Bool,
    'from' : User,
    'memo' : Memo,
    'subaccount' : IDL.Opt(SubAccount),
    'amount' : Balance,
  });
  const TransferResponse = IDL.Variant({
    'ok' : Balance,
    'err' : IDL.Variant({
      'CannotNotify' : AccountIdentifier,
      'InsufficientBalance' : IDL.Null,
      'InvalidToken' : TokenIdentifier,
      'Rejected' : IDL.Null,
      'Unauthorized' : AccountIdentifier,
      'Other' : IDL.Text,
    }),
  });
  return IDL.Service({
    'acceptCycles' : IDL.Func([], [], []),
    'addAsset' : IDL.Func(
        [AssetHandle, IDL.Nat32, IDL.Text, IDL.Text, IDL.Text],
        [],
        [],
      ),
    'addThumbnail' : IDL.Func([AssetHandle, IDL.Vec(IDL.Nat8)], [], []),
    'adminKillHeartbeat' : IDL.Func([], [], []),
    'adminKillHeartbeatExtra' : IDL.Func([IDL.Text], [], []),
    'adminStartHeartbeat' : IDL.Func([], [], []),
    'adminStartHeartbeatExtra' : IDL.Func([IDL.Text], [], []),
    'allPayments' : IDL.Func(
        [],
        [IDL.Vec(IDL.Tuple(IDL.Principal, IDL.Vec(SubAccount__1)))],
        ['query'],
      ),
    'allSettlements' : IDL.Func(
        [],
        [IDL.Vec(IDL.Tuple(TokenIndex, Settlement))],
        ['query'],
      ),
    'assetTokenMap' : IDL.Func(
        [],
        [IDL.Vec(IDL.Tuple(AssetHandle, TokenIndex))],
        ['query'],
      ),
    'assetsToTokens' : IDL.Func(
        [IDL.Vec(AssetHandle)],
        [IDL.Vec(TokenIndex)],
        ['query'],
      ),
    'availableCycles' : IDL.Func([], [IDL.Nat], ['query']),
    'balance' : IDL.Func([BalanceRequest], [BalanceResponse], ['query']),
    'bearer' : IDL.Func([TokenIdentifier__1], [Result_7], ['query']),
    'clearPayments' : IDL.Func([IDL.Principal, IDL.Vec(SubAccount__1)], [], []),
    'cronCapEvents' : IDL.Func([], [], []),
    'cronDisbursements' : IDL.Func([], [], []),
    'cronSettlements' : IDL.Func([], [], []),
    'details' : IDL.Func([TokenIdentifier__1], [Result_8], ['query']),
    'extensions' : IDL.Func([], [IDL.Vec(Extension)], ['query']),
    'failedSales' : IDL.Func(
        [],
        [IDL.Vec(IDL.Tuple(AccountIdentifier__1, SubAccount__1))],
        ['query'],
      ),
    'getMinter' : IDL.Func([], [IDL.Principal], ['query']),
    'getRegistry' : IDL.Func(
        [],
        [IDL.Vec(IDL.Tuple(TokenIndex, AccountIdentifier__1))],
        ['query'],
      ),
    'getTokens' : IDL.Func(
        [],
        [IDL.Vec(IDL.Tuple(TokenIndex, Metadata))],
        ['query'],
      ),
    'historicExport' : IDL.Func([], [IDL.Bool], []),
    'http_request' : IDL.Func([HttpRequest], [HttpResponse], ['query']),
    'initCap' : IDL.Func([], [], []),
    'list' : IDL.Func([ListRequest], [Result_3], []),
    'listings' : IDL.Func(
        [],
        [IDL.Vec(IDL.Tuple(TokenIndex, Listing, Metadata))],
        ['query'],
      ),
    'lock' : IDL.Func(
        [TokenIdentifier__1, IDL.Nat64, AccountIdentifier__1, SubAccount__1],
        [Result_7],
        [],
      ),
    'metadata' : IDL.Func([TokenIdentifier__1], [Result_6], ['query']),
    'payments' : IDL.Func([], [IDL.Opt(IDL.Vec(SubAccount__1))], ['query']),
    'reserve' : IDL.Func(
        [IDL.Nat64, IDL.Nat64, AccountIdentifier__1, SubAccount__1],
        [Result_5],
        [],
      ),
    'retreive' : IDL.Func([AccountIdentifier__1], [Result_4], []),
    'saleTransactions' : IDL.Func([], [IDL.Vec(SaleTransaction)], ['query']),
    'salesSettings' : IDL.Func(
        [AccountIdentifier__1],
        [SaleSettings],
        ['query'],
      ),
    'salesSettlements' : IDL.Func(
        [],
        [IDL.Vec(IDL.Tuple(AccountIdentifier__1, Sale))],
        ['query'],
      ),
    'setMinter' : IDL.Func([IDL.Principal], [], []),
    'settle' : IDL.Func([TokenIdentifier__1], [Result_3], []),
    'settlements' : IDL.Func(
        [],
        [IDL.Vec(IDL.Tuple(TokenIndex, AccountIdentifier__1, IDL.Nat64))],
        ['query'],
      ),
    'stats' : IDL.Func(
        [],
        [IDL.Nat64, IDL.Nat64, IDL.Nat64, IDL.Nat64, IDL.Nat, IDL.Nat, IDL.Nat],
        ['query'],
      ),
    'supply' : IDL.Func([TokenIdentifier__1], [Result_2], ['query']),
    'toAddress' : IDL.Func(
        [IDL.Text, IDL.Nat],
        [AccountIdentifier__1],
        ['query'],
      ),
    'tokens' : IDL.Func([AccountIdentifier__1], [Result_1], ['query']),
    'tokens_ext' : IDL.Func([AccountIdentifier__1], [Result], ['query']),
    'transactions' : IDL.Func([], [IDL.Vec(Transaction)], ['query']),
    'transfer' : IDL.Func([TransferRequest], [TransferResponse], []),
  });
};

export default function App() {
  const provider = getProvider();
  const [logs, setLogs] = useState<string[]>([]);
  const addLog = useCallback(
    (log: string) => setLogs((logs) => [...logs, "> " + log]),
    []
  );
  const connection = new Connection(NETWORK);
  const [, setConnected] = useState<boolean>(false);
  const [publicKey, setPublicKey] = useState<PublicKey | null>(null);
  const [dfinityPrincipal, setDfinityPrincipal] = useState<String>(null);
  const [nftList, setNFTList] = useState<Array<Object>>([]);
  const [selectedNFT, setSelectedNFT] = useState<Number>(null);
  const [mktnftList, setmktNFTList] = useState<Array<Object>>([]);
  const [mktselectedNFT, setmktSelectedNFT] = useState<Number>(null);

  const NFT_TYPE_DIP20 = 'dip20';
  const NFT_TYPE_EXT = 'ext';
  const NFT_LIST = {
    'cap-crown': {
      'canisterId': 'vlhm2-4iaaa-aaaam-qaatq-cai',
      'idl': DIP20IDL,
      'type': NFT_TYPE_DIP20,
    },
    'boxy': {
       'canisterId': 's36wu-5qaaa-aaaah-qcyzq-cai',
       'idl': EXTIDL,
       'type': NFT_TYPE_EXT,
    }
  }
  const NFT_FN = {
    listTransactions: async function(nftType, price, fromPrincipal) { 
      var marketPlaceId = await getMarketPlaceId();
      addLog('MarketPlace Id: '+ marketPlaceId);
      return [{
        idl: EXTIDL,
        canisterId: boxyId,
        methodName: 'transfer',
        args:[{
          to: { principal: Principal.from(marketPlaceId) },
          from: { principal: Principal.from(fromPrincipal) },
          token: getTokenIdentifier(boxyId, selectedNFT),
          amount: BigInt(1),
          memo: new Array(32).fill(2),
          notify: false,
          subaccount: [],
        }],
        onSuccess: async (res) => {
          addLog('transferred NFT successfully');
        },
        onFail: (res) => {
          addLog('transfer NFT error' + res);
        }
      }]   
    },
    unlistTransactions: async function(nftType, toPrincipal){
      var marketPlaceId = await getMarketPlaceId();
      addLog('MarketPlace Id: '+ marketPlaceId);
      return []   
    },
    buyTransactions: async function(){
      const TRANSFER_FEE = {
        idl: nnsPartialInterfaceFactory,
        canisterId: nnsCanisterId,
        methodName: 'transfer',
        args: {
          to: getAccountIdentifier(Principal.from(await getMarketPlaceId())),
          fee: { e8s: BigInt(10000) },
          amount: { e8s: BigInt(100000) },
          memo: new Array(32).fill(1),
          from_subaccount: [], // For now, using default subaccount to handle ICP
          created_at_time: [],
        },
        onSuccess: async (res) => {
          addLog('transferred icp successfully');
        },
        onFail: (res) => {
          // addLog('transfer icp error' + res);
        },
      };
      return [TRANSFER_FEE]
    }
  }
  const NFT_SELECTED = 'boxy'

  useEffect(() => {
    if (!provider) return;
    // try to eagerly connect
    provider.connect({ onlyIfTrusted: true }).catch((err) => {
      // fail silently
    });
    provider.on("connect", (publicKey: PublicKey) => {
      setPublicKey(publicKey);
      setConnected(true);
      addLog("[connect] " + publicKey?.toBase58());
    });
    provider.on("disconnect", () => {
      setPublicKey(null);
      setConnected(false);
      //addLog("[disconnect] 👋");
    });
    provider.on("accountChanged", (publicKey: PublicKey | null) => {
      setPublicKey(publicKey);
      if (publicKey) {
        addLog("[accountChanged] Switched account to " + publicKey?.toBase58());
      } else {
        //addLog("[accountChanged] Switched unknown account");
        // In this case, dapps could not to anything, or,
        // Only re-connecting to the new account if it is trusted
        // provider.connect({ onlyIfTrusted: true }).catch((err) => {
        //   // fail silently
        // });
        // Or, always trying to reconnect
        provider
          .connect()
          .then(() => addLog("[accountChanged] Reconnected successfully"))
          .catch((err) => {
            //addLog("[accountChanged] Failed to re-connect: " + err.message);
          });
      }
    });
    return () => {
      provider.disconnect();
    };
  }, [provider, addLog]);
  if (!provider) {
    // return <h2>Could not find a provider</h2>;
  }
  const fromwallet = new Uint8Array([193,47,61,157,108,137,241,249,71,124,31,187,115,154,174,79,250,125,148,26,6,137,84,38,144,25,87,224,58,133,116,157,6,162,23,15,133,42,247,207,234,38,19,135,17,169,74,217,243,221,6,10,22,108,74,177,145,104,232,3,97,31,114,187]);  //address : stone
  let marketplaceWallet = solanaWeb3.Keypair.fromSecretKey(fromwallet)
  let receiver;
  receiver = provider  

  const sentamount = 0.01
  const recevieamount = 0.02

             
	
  const getTxSignature = async (signer, instruction1, blockhash) => 
  {
      addLog('getTxSignature in :'+signer.publicKey)
      const tx = new solanaWeb3.Transaction()
      tx.add(...instruction1)

      tx.recentBlockhash = blockhash
      tx.feePayer = receiver.publicKey
      tx.sign(signer)

      tx.signatures.forEach((signature) => {
        addLog("signature :"+signature.publicKey.toBase58());
      });
      const signature = tx.signatures.find(signature => signature.publicKey.toBase58() === signer.publicKey.toBase58())
      return signature
  }  
  
  const ClearText = async () => {
    try 
    {    
      const myNode = document.getElementById("divLog");
      myNode.innerHTML = '';      
      
      
    } catch (err) {
      console.warn(err);
      addLog("[error] ClearText: " + JSON.stringify(err));
    }
  };

  const listNFT  = async () => {
    addLog("Selected NFT: " + selectedNFT?.toString());
    if(!!selectedNFT){
      var transactions = await NFT_FN.listTransactions(NFT_TYPE_EXT, 1, dfinityPrincipal);
      if(!!mWindow.ic && !!mWindow.ic.plug){
        await mWindow.ic.plug.batchTransactions(transactions);
        await fetchNFTs();
      }   
    }
  };

  const unlistNFT  = async () => {
    addLog("Selected MarketPlace NFT: " + mktselectedNFT?.toString());
    if(!!mktselectedNFT){
      await mktTransferNFT(NFT_TYPE_EXT, boxyId, dfinityPrincipal, mktselectedNFT);
      await fetchNFTs();
    }
    addLog("Unlisted NFT:" + mktselectedNFT);
  };

  const buyNFT  = async () => {
    addLog("Selected MarketPlace NFT: " + mktselectedNFT?.toString());
    if(!!mWindow.ic && !!mWindow.ic.plug && !!mktselectedNFT){
     const params = {
        to: await getMarketPlaceId(),
        amount: 100000,
        memo: '123451231231',
     };
     const result = await mWindow.ic.plug.requestTransfer(params);
    //  await mWindow.ic.plug.batchTransactions(await NFT_FN.buyTransactions())
     await mktTransferNFT(NFT_TYPE_EXT, boxyId, dfinityPrincipal, mktselectedNFT);
     await fetchNFTs();
    }
  };

  const fetchNFTs = async () => {
    const BoxyActor = await mWindow.ic.plug.createActor({
      canisterId: boxyId,
      interfaceFactory: EXTIDL
    });
    const marketPlaceId = await getMarketPlaceId();
    var tokenList = (await BoxyActor.tokens(
      getAccountIdentifier(dfinityPrincipal)
    )).ok;
    var mkttokenList = (await BoxyActor.tokens(
      getAccountIdentifier(marketPlaceId)
    )).ok;
    var nftList = [];
    var mktnftList = [];
    if(!!tokenList){
      tokenList.forEach((token) => {
        var tokenId = getTokenIdentifier(boxyId, token);
        nftList.push({
          thumbnail: `https://${boxyId}.raw.ic0.app/?type=thumbnail&tokenid=${tokenId}`,
          tokenIndex: token
        })
      }); 
      setNFTList(nftList);
    }
    if(!!mkttokenList){
      mkttokenList.forEach((token) => {
        var tokenId = getTokenIdentifier(boxyId, token);
        mktnftList.push({
          thumbnail: `https://${boxyId}.raw.ic0.app/?type=thumbnail&tokenid=${tokenId}`,
          tokenIndex: token
        })
      });
      setmktNFTList(mktnftList);
    }
  }

  return (
    <div className="App">
      <main>
        <h1>ICP Market Place Demo</h1>
        {provider && dfinityPrincipal ? (
          <>
            <div>
              <pre>Connected as</pre>
              <br />
              <pre>{dfinityPrincipal}</pre>
              <br />
            </div>            
            <div>
              <pre>Selected NFT Canister</pre>
              <br />
              <pre>{NFT_SELECTED}</pre>
              <br />
            </div>            
            <div onChange={event => setSelectedNFT(event.target['value'])}>
              <pre>NFT List</pre>
              <br />
              <table>
              <tbody>
                <tr>
              {nftList.map((nft, index) => (
                  <td>
                  <img src={nft["thumbnail"]} width='100px' height='100px'/>
                  <pre>token#:{nft['tokenIndex']}</pre>
                  <input type="radio" name="selectedNFT" value={nft['tokenIndex']}/>
                  </td>              
               ))}
                </tr>
                </tbody>
               </table>
              <br />
            </div>
            <div onChange={event => setmktSelectedNFT(event.target['value'])}>
              <pre>Marketplace NFT List</pre>
              <br />
              <table>
              <tbody>
                <tr>
              {mktnftList.map((nft, index) => (
                  <td>
                  <img src={nft["thumbnail"]} width='100px' height='100px'/>
                  <pre>token#:{nft['tokenIndex']}</pre>
                  <input type="radio" name="mktselectedNFT" value={nft['tokenIndex']}/>
                  </td>              
               ))}
                </tr>
                </tbody>
               </table>
              <br />
            </div>            
            <button onClick={async () => await listNFT()}>
              List NFT
            </button>
            <button onClick={async () => await unlistNFT()}>
              Unlist NFT
            </button>
            <button onClick={async () => await buyNFT()}>
              Buy NFT
            </button>
            <button onClick={() => ClearText()}>
              Clear{" "}
            </button>
          </>
        ) : (
          <>
            <button
              onClick={async () => {
                try {
                  if ("ic" in window) {
                    const whiteList = [boxyId, nnsCanisterId];
                    var IC = mWindow.ic;
                    await IC.plug.requestConnect({whitelist:whiteList});
                    var publicKey = IC.plug.principalId;
                    addLog(publicKey.toString());
                    await IC.plug.agent.fetchRootKey();
                    console.log(IC.plug.createActor);
                    // const NNSUiActor = await IC.plug.createActor({
                    //   canisterId: nnsCanisterId,
                    //   interfaceFactory: nnsPartialInterfaceFactory
                    // });
                    var principalIdObject = await IC.plug.getPrincipal();

                    setDfinityPrincipal(publicKey);

                    const BoxyActor = await IC.plug.createActor({
                      canisterId: boxyId,
                      interfaceFactory: EXTIDL
                    });
                    const marketPlaceId = await getMarketPlaceId();
                    var tokenList = (await BoxyActor.tokens(
                      getAccountIdentifier(publicKey)
                    )).ok;
                    var mkttokenList = (await BoxyActor.tokens(
                      getAccountIdentifier(marketPlaceId)
                    )).ok;
                    var nftList = [];
                    var mktnftList = [];
                    if(!!tokenList){
                      tokenList.forEach((token) => {
                        var tokenId = getTokenIdentifier(boxyId, token);
                        nftList.push({
                          thumbnail: `https://${boxyId}.raw.ic0.app/?type=thumbnail&tokenid=${tokenId}`,
                          tokenIndex: token
                        })
                      }); 
                      setNFTList(nftList);
                    }
                    if(!!mkttokenList){
                      mkttokenList.forEach((token) => {
                        var tokenId = getTokenIdentifier(boxyId, token);
                        mktnftList.push({
                          thumbnail: `https://${boxyId}.raw.ic0.app/?type=thumbnail&tokenid=${tokenId}`,
                          tokenIndex: token
                        })
                      });
                      setmktNFTList(mktnftList);
                    }
                    console.log('accountId: ' + getAccountIdentifier(publicKey));
                    console.log('tokenId: ' + getTokenIdentifier("s36wu-5qaaa-aaaah-qcyzq-cai", 5041));
                    // addLog(JSON.stringify(stats));
                  }
                } catch (err) {
                  console.warn(err);
                  addLog("[error] connect: " + JSON.stringify(err));
                }
              }}
            >
              Connect to Plug
            </button>
            <button
              onClick={async () => {
                try {
                  var identity = await StoicIdentity.load();
                  if(!identity){
                    identity = await StoicIdentity.connect();
                  }
                  // addLog(identity.getPrincipal().toString());
                  // addLog(await identity.sign("hello world"));
                  const BoxyActor = Actor.createActor(EXTIDL,
                     {
                      agent: new HttpAgent({
                        identity: identity
                      }),
                      canisterId: "bxdf4-baaaa-aaaah-qaruq-cai"
                    }
                  );
                  var stats = await BoxyActor.getTokens();
                  addLog(JSON.stringify(stats));
                } catch (err) {
                  console.warn(err);
                  addLog("[error] connect: " + JSON.stringify(err));
                }
              }}
            >
              Connect to Stoic
            </button>
          </>
        )}
      </main>
      <footer id="divLog" className="logs">
        {logs.map((log, i) => (
          <div className="log" key={i}>
            {log}
          </div>
        ))}
      </footer>
    </div>
  );
}

