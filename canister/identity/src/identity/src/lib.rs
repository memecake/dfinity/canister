use ic_cdk::export::candid;
use ic_cdk_macros::*;

static mut USER: Option<candid::Nat> = None;

#[init]
fn init() {
    unsafe {
        USER = Some(candid::Nat::from(0));
    }
}

#[update]
fn increment() -> () {
    unsafe {
        USER.as_mut().unwrap().0 += 1u64;
    }
}

#[query]
fn get() -> candid::Nat {
    unsafe { USER.as_mut().unwrap().clone() }
}

#[update]
fn set(input: candid::Nat) -> () {
    unsafe {
        USER.as_mut().unwrap().0 = input.0;
    }
}
