import Prim "mo:prim";
import Text "mo:base/Text";
import Map "mo:base/HashMap";
import Principal "mo:base/Principal";
import Option "mo:base/Option";

import UUID "mo:uuid/UUID";
import Source "mo:uuid/Source";
import AsyncSource "mo:uuid/async/SourceV4";
import XorShift "mo:rand/XorShift";

import Utils "./Utils";

actor {

  // types
  type AccountIdentifier = Text;
  type Token = Text;
  type LoginResponse = {
    token: Token;
    user: Principal;
  };
    
  // initialize params for token generation
  private let ae = AsyncSource.Source();
  private let rr = XorShift.toReader(XorShift.XorShift64(null));
  private stable var originId : [Nat8] = [0, 0, 0, 0, 0, 0];
  private let se = Source.Source(rr, originId);
  // App params
  private stable var identityAdmin : Principal = Principal.fromText("d7flx-ndkun-kiqzj-qv3j5-jvbvv-rlsqu-j6nxb-mz7r2-f72lk-fqnkz-5ae");
  
  // User data
  let userTokens = Map.HashMap<AccountIdentifier, Token>(0, Text.equal, Text.hash);
  
  // helpers
  func p2a(p: Principal) : Text {
      Utils.accountToText(Utils.principalToAccount(p))
  };

  public shared(msg) func setOriginId(newOriginId: [Nat8]): async() {
      assert(msg.caller == identityAdmin);
      originId := newOriginId;
  };

  public shared(msg) func setIdentityAdmin(admin : Principal): async () {
      assert(msg.caller == identityAdmin);
      identityAdmin := admin;
  };

  public shared(msg) func verifyToken(user:Principal, token : Token): async Bool {
      assert(msg.caller == identityAdmin);
      let aId = p2a(user);
      let userToken = userTokens.get(aId);
      switch(?userToken){
       case null {
         return false;
       };
       case _ {
         return Option.unwrap(userToken) == token;
       }
      };
  };

  public shared(msg) func login() : async LoginResponse {
     let genId = await ae.new();
     let aId = p2a(msg.caller);

     let token = Text.map(UUID.toText(genId), Prim.charToLower);
     userTokens.put(aId, token);

     let loginResponse: LoginResponse = {
       token = token;
       user = msg.caller;
     };

     return loginResponse;
  };
};
