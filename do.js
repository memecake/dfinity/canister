global.fetch = require('node-fetch');
const Identity = require("@dfinity/identity");
const hdkey = require("hdkey");
const fs = require("fs");
const { Actor, HttpAgent, SignIdentity } = require('@dfinity/agent');
const {EXTIDL} = require('./extidl.js');

const { Secp256k1KeyIdentity } = Identity;

const bip39 = require("bip39");

const phrase = fs.readFileSync("seed.txt").toString().trim();

const identityFromSeed = async (phrase) => {
  const seed = await bip39.mnemonicToSeed(phrase);
  const root = hdkey.fromMasterSeed(seed);
  const addrnode = root.derive("m/44'/223'/0'/0/0");

  return Secp256k1KeyIdentity.fromSecretKey(addrnode.privateKey);
};

identityFromSeed(phrase).then(async (identity) => {
  console.log(identity.getPrincipal().toString());
  const host = 'https://boundary.ic0.app/';
  const actor = Actor.createActor(EXTIDL, {
    agent: new HttpAgent({
      host,
      identity,
    }),
    canisterId: "bxdf4-baaaa-aaaah-qaruq-cai"
  });
  var metaData = await actor.getTokens();
});
